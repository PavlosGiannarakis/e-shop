<div id="welc_div">
<h1>Welcome to Flowers on Wheels!</h1>
<p>We hope you enjoy shopping with us! <br><br>
Our goal is to make your day brighter with a bouquet of flowers,
search our website for all our special occasion bouquet's and keep an eye out for our Special offers this October.
</p>
</div>
<div id="products_show">
<?php
include_once 'confi.php';
$sql = "SELECT * FROM products";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
		echo "<div id = '". $row['id'] ."' class='product_show'>";
		echo "<img class='prod_img' src='images/". $row['imageName'] ."' alt='". $row['title'] ."'>";
		echo "<div class='prod_desc'><b>". $row['title'] ."</b><br><br><b>Price:</b>
		<i>£". $row['price'] ."</i><br><br>Available for delivery.</div> </div>";
    }
} else {
    echo "0 results";
}
?>
</div>