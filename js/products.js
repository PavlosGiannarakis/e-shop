$( function() {
	
	$("#content").on('click','#product_list .products', function() {
		$id = $(this).attr('id');
		$("#content").load("products.php?pid="+ $id);
		$(".menubar .m_item").removeClass('liClicked');
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products", position: 'pid='+$id}, null, '/TestProj/product.php?pid='+ $id);
	});
	
	$("#content").on('click','#products_show .product_show', function() {
		$id = $(this).attr('id');
		$("#content").load("products.php?pid="+ $id);
		$(".menubar .m_item").removeClass('liClicked');
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products", position: 'pid='+$id}, null, '/TestProj/product.php?pid='+ $id);
	});
	
	$("#content").on('click', '.basket_button2', function(){
		window.location.href = "http://localhost/TestProj/product";
	});
	
	$("#content").on('click', '.basket_button', function(){
		$bid = $(this).attr('name');
		$.post("atb.php", {pid: $bid});
		alert("Successfully added to your basket.");
	});
	
});