$(function(){
	$loc = $(location).attr('href');
	//var loc = window.location.href;	
	if ( $loc == "http://localhost/TestProj/")
	{
		$(".menubar .m_item:first-child").addClass('liClicked');
		history.pushState({ data : "index"}, null, null);
	}
	if ( $loc.indexOf("index") > -1)
	{
		$(".menubar .m_item:first-child").addClass('liClicked');
		history.pushState({ data : "index"}, null, null);
	}
	
	if ( $loc == "http://localhost/TestProj/product")
	{
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products"}, null, null);
	}
	if ( $loc.indexOf("product") > -1 && $loc.indexOf("?pid") > -1)
	{
		$url = $loc.split('?');
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products", position: $url[1]}, null, null);
	}
	else if ( $loc.indexOf("product") > -1 && $loc.indexOf("?search") > -1)
	{
		$url = $loc.split('?');
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products", position: $url[1]}, null, null);
	}
	else if( $loc.indexOf("product") > -1  )
	{
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
		history.pushState({ data : "products"}, null, null);
	}
	
	if ( $loc == "http://localhost/TestProj/occasion")
	{
		$(".menubar .m_item:nth-child(3)").addClass('liClicked');
		history.pushState({ data : "occasion"}, null, null);
	}
	if ( $loc.indexOf("occasion") > -1)
	{
		$(".menubar .m_item:nth-child(3)").addClass('liClicked');
		history.pushState({ data : "occasion"}, null, null);
	}
	
	if ( $loc == "http://localhost/TestProj/about")
	{
		$(".menubar .m_item:nth-child(4)").addClass('liClicked');
		history.pushState({ data : "about"}, null, null);
	}
	if ( $loc.indexOf("about") > -1)
	{
		$(".menubar .m_item:nth-child(4)").addClass('liClicked');
		history.pushState({ data : "about"}, null, null);
	}
	
	if ( $loc == "http://localhost/TestProj/contact")
	{
		$(".menubar .m_item:last-child").addClass('liClicked');
		history.pushState({ data : "contact"}, null, null);
	}
	if ( $loc.indexOf("contact") > -1)
	{
		$(".menubar .m_item:last-child").addClass('liClicked');
		history.pushState({ data : "contact"}, null, null);
	}
	
	$(".menubar .m_item").click(function() {
	$(".menubar .m_item").removeClass('liClicked');
	$(this).toggleClass('liClicked');
	if($(this).is(':first-child'))
	{		
		$( "#content" ).load( "homepage.php" );
		
		history.pushState({ data : "index"}, null, '/TestProj/');
	}
	else if($(this).is(':nth-child(2)'))
	{
		$( "#content" ).load( "products.php" );
		history.pushState({ data : "products"}, null, '/TestProj/product');
	}
	else if($(this).is(':nth-child(3)'))
	{
		$( "#content" ).load( "html/occasion.html" );
		history.pushState({ data : "occasion"}, null, '/TestProj/occasion');
	}
	else if($(this).is(':nth-child(4)'))
	{
		$( "#content" ).load( "html/about.html" );
		history.pushState({ data : "about"}, null, '/TestProj/about');
	}
	else if($(this).is(':last-child'))
	{
		$( "#content" ).load( "html/contact.html" );
		history.pushState({ data : "contact"}, null, '/TestProj/contact');
	}
	});
	
	window.addEventListener('popstate', function(e) {
	if(e.state.data == "index")
	{
		$( "#content" ).load("homepage.php");	
	}
	else if(e.state.data == "products")
	{
		if(e.state.position !== undefined && e.state.position !== null)
		{
			$( "#content" ).load("products.php?" + e.state.position);
		}
		else
		{
			$( "#content" ).load("products.php");
		}
	}
	else if(e.state.data == "register")
	{
		window.location.href = "http://localhost/TestProj/register";
	}
	else
	{
		$( "#content" ).load("html/" + e.state.data + ".html");
	}
	$(".menubar li").removeClass('liClicked');
	if(e.state.data == "index")
	{
		$(".menubar .m_item:first-child").addClass('liClicked');
	}
	else if(e.state.data == "products")
	{
		$(".menubar .m_item:nth-child(2)").addClass('liClicked');
	}
		else if(e.state.data == "occasion")
	{
		$(".menubar .m_item:nth-child(3)").addClass('liClicked');
	}
	else if(e.state.data == "about")
	{
		$(".menubar .m_item:nth-child(4)").addClass('liClicked');
	}
	else if(e.state.data == "contact")
	{
		$(".menubar .m_item:last-child").addClass('liClicked');
	}
});
	
});