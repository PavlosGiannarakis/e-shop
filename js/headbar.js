$(function(){
	$("#reg_btn").click(function(){
		window.location.href = "http://localhost/TestProj/register";
		//$(".menubar li").removeClass('liClicked');
		history.pushState({ data : "register"}, null, '/TestProj/register');
	});
	$("#lgi_btn").click(function(){
		$( "#content" ).load( "html/log_in.html" );
		history.pushState({ data : "log_in"}, null, '/TestProj/log_in');
	});
	$("#lgo_btn").click(function(){
		window.location.href = "http://localhost/TestProj/logout";
	});
	$("#bskt_btn").click(function(){
		window.location.href = "http://localhost/TestProj/basket";
	});
	$(".not_impl").click(function() {
		$( "#content" ).load( "html/puc.html" );
		history.pushState({ data : "puc"}, null, 'puc');
	});
	
	$("#h_img").click(function() {
		$(".menubar .m_item").removeClass('liClicked');
		$(".menubar .m_item:first-child").addClass('liClicked');
		$( "#content" ).load( "homepage.php" );
		history.pushState({ data : "index"}, null, '/TestProj/');
	});
});