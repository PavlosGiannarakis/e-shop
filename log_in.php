<!DOCTYPE html>
<html lang="en">
<?php
include_once 'html/header.html';
?>
<body>
<div id="mainBody">
<?php
include_once 'headerContent.php';
echo "<div id='content'>";
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	include_once 'confi.php';
	
	$id = isset($_POST['id']) ? $_POST['id'] : '';	
	$pass = isset($_POST['pwd']) ? $_POST['pwd'] : '';
		
	$sql = "SELECT * FROM credentials WHERE id = '$id' AND pass = '$pass'";
	$result = mysqli_query($conn, $sql);
	
	if (mysqli_num_rows($result) > 0) {
		$sql2 = "SELECT fname FROM userdetails WHERE id = '$id'";
		$result2 = mysqli_query($conn,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$fname = $row2['fname'];
		

		$sql3 = "SELECT * FROM basket WHERE id = '$id'";
		$result3 = mysqli_query($conn,$sql3);
		if (mysqli_num_rows($result3) > 0) {
			$row3 = mysqli_fetch_assoc($result3);
			$m_arr = array_merge(unserialize($row3['basket']),$_SESSION['basket']);
			$_SESSION['basket'] = $m_arr;
		}
		$_SESSION['name'] = $fname;
		$_SESSION['id'] = $id;
		header('location: index.php');
	}
	else 
	{
		include_once 'html/log_in.html';
		echo "<h3>An error happened during login, please try again.</h3>";
	}
	mysqli_close($conn);	
}
else
{
	include_once 'html/log_in.html';
}
echo "</div>";
include_once 'html/footer.html';
?>
</div>
</body>
</html>