<?php
include_once 'confi.php';
if(isset($_GET['search']))
{
	$sql = "SELECT * FROM products";
	$result = mysqli_query($conn, $sql);
	$Org_src = $_GET['search'];
	$search = strtolower($Org_src);
	$sr = 0;
	$rows = mysqli_num_rows($result);
	if (mysqli_num_rows($result) > 0) {
		echo "<div id='product_list'>";
		while($row = mysqli_fetch_assoc($result)) {
			$temp = explode(" ",$row['title']);
			$t_arr = array_map('strtolower',$temp);
			if(in_array($search, $t_arr))
			{
				echo "<div id= '". $row['id'] ."' class='products'>
				<img class='pimg' src='images/". $row['imageName'] .
				"' alt='". $row['title'] ."'>
				<div class='prod_description'>
				<div>
				<h3>". $row['title'] ."</h3>
				Available for delivery <b>today</b>.
				<br><br>
				". $row['description'] ."
				<br><br>
				Price:  <i>£". $row['price'] ."</i>
				<br><br>
				Extras: <i>Yet to come!</i>
				</div>
				</div>
				</div>";
			}
			else
			{
				$sr++;
			}
		}
		if($rows == $sr)
		{
			echo "No results found for:<br> <b>" . $Org_src . "</b>";
		}
		echo "</div>";
	}
	else
	{	
		header('location: product.php');
		echo "An error occured.";
	}
}
else if (isset($_GET['pid']))
{
	$pid = $_GET['pid'];

	$sql = "SELECT * FROM products WHERE id = '$pid'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);	
		echo "<div class='products2'>
		<img class='img' src='images/". $row['imageName'] .
		"' alt='". $row['title'] ."'>
		<div class='prod_description'>
		<div>
		<h3>". $row['title'] ."</h3>
		Available for delivery <b>today</b>.
		<br><br>
		". $row['description'] ."
		<br><br>
		Price:  <i>£". $row['price'] ."</i>
		<br><br>
		Extras: <i>Yet to come!</i>
		</div>
		<button class='basket_button' name= ". $row['id'] .">Add to Basket</Button>
		<button class='basket_button2'>Cancel</Button>
		</div>
		</div>";
	}
	else
	{
		header('location: product.php');
		echo "An error occured.";	
	}
}
else
{
	$sql = "SELECT * FROM products";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		echo "<div id='product_list'>";
		while($row = mysqli_fetch_assoc($result)) {
			echo "<div id= '". $row['id'] ."' class='products'>
			<img class='pimg' src='images/". $row['imageName'] .
			"' alt='". $row['title'] ."'>
			<div class='prod_description'>
			<div>
			<h3>". $row['title'] ."</h3>
			Available for delivery <b>today</b>.
			<br><br>
			". $row['description'] ."
			<br><br>
			Price:  <i>£". $row['price'] ."</i>
			<br><br>
			Extras: <i>Yet to come!</i>
			</div>
			</div>
			</div>";
		}
		echo "</div>";
	}
	else
	{	
		header('location: product.php');
		echo "An error occured.";
	}
}
?>