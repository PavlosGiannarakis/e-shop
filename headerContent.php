<div id="header">

<img id='h_img' class='img' src='images/main_logo.png' alt='Welcome to Flowers on Wheels'>

<div id="headerRight">

<div class="hr_div">
<div id="h_but">
<?php

session_start();
if(!isset($_SESSION['basket']))
{
	$_SESSION['basket'] = array();
}

if(isset($_SESSION['id'])) {
	echo "Welcome, <b>". $_SESSION['name'] ."</b>
	<button class='text_button not_impl'>My Account</button>
	<button class='text_button' id='lgo_btn'>Log Out</button>";
}
else
{
	echo "<button class='text_button not_impl'>Give Feedback!</button>
	<button class='text_button' id='reg_btn'>Register</button>
	<button class='text_button' id='lgi_btn'>Sign In</button>";
}
?>
<button class="text_button" id="bskt_btn">Basket  <img class='B_img' src='images/basket.png' alt='Your Basket'></button>
</div>
</div>

<div class="hr_div">
    <form action="product.php" method="GET">
	  <input class="searchbar" type="text" name="search" placeholder="Search..">
	</form>
</div>

</div>

</div>

<ul class="menubar">
  <li class="m_item" onclick="">Home
  <li class="m_item" onclick="">Products
  <li class="m_item" onclick="">Occasion &#8595;
  <ul class="submenu">
  <li class="s_item">Gift</li>
  <li class="s_item">Wedding</li>
  <li class="s_item">Anniversary</li>
  </ul>
  </li>
  <li class="m_item" onclick="">About
  <li class="m_item" onclick="">Contact
</ul>