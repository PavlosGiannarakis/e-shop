<!DOCTYPE html>
<html lang="en">
<?php
include_once 'html/header.html';
?>
<body>
<div id="mainBody">
<?php
include_once 'headerContent.php';
echo "<div id='content'>";
echo "<div id='bskt_cont'>";
echo "<h2>Your Basket:</h2>";
include_once 'confi.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$_SESSION['basket'] = array();
}
$mybasket = $_SESSION['basket'];
$total_items = count($mybasket);
$total_price = 0;

if(empty($mybasket))
{
	echo "Your basket is empty. Go add something to it.<br>";
}
else
{
	$sql = "SELECT * FROM products";
	$result = mysqli_query($conn, $sql);
	$res_arr = array_count_values($mybasket);
	echo "<div id='product_list'>";
		while($row = mysqli_fetch_assoc($result)) {
			if(in_array(($row['id']), $mybasket))
			{
				echo "<div id= '". $row['id'] ."' class='products'>
				<img class='pimg' src='images/". $row['imageName'] .
				"' alt='". $row['title'] ."'>
				<div class='prod_description'>
				<div>
				<h3>". $row['title'] ."</h3>
				Available for delivery <b>today</b>.
				<br><br>
				<i>Count:</i>
				". $res_arr[$row['id']] ."
				<br><br>
				Price:  <i>£". $row['price'] ."</i>
				<br><br>
				Extras: <i>Yet to come!</i>
				</div>
				</div>
				</div>";
				$total_price = $total_price + (intval($res_arr[$row['id']]) * intval($row['price']));
			}
		}
	echo "</div>";
}
echo "<br><div id = 'bskt_sum'>
Total Items: ". $total_items ."
<br> Total Price: ". $total_price ."
<br><br>";
if(isset($_SESSION['id']))
{
	$id = $_SESSION['id'];
	$sql = "SELECT * FROM userdetails WHERE id = '$id'";
	$result = mysqli_query($conn,$sql);
	$row = mysqli_fetch_assoc($result);
	echo "Delivery to: ". $row['fname'] ." ". $row['sname'] ."<br>";
	echo "Devilery at: ". $row['address']."<br>";
}
echo"<button id='cont_bskt' class='emb_btn'>Continue</button></div>";
echo"<button id='empt_bskt' class='emb_btn'>Empty basket</button></div>";
echo "</div>";

include_once 'html/footer.html';
echo "</div>";
?>
</div>
</body>
</html>