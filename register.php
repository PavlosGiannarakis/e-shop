<!DOCTYPE html>
<html lang="en">
<?php
include_once 'html/header.html';
?>
<body>
<div id="mainBody">
<?php
include_once 'headerContent.php';
echo "<div id='content'>";
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	include_once 'confi.php';

	$fname = isset($_POST['fname']) ? trim($_POST['fname']) : '';
	$sname = isset($_POST['sname']) ? trim($_POST['sname']) : '';
	$address = isset($_POST['address']) ? trim($_POST['address']) : '';
	$email = isset($_POST['email']) ? trim($_POST['email']) : '';
	$phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
	$pass = isset($_POST['password']) ? trim($_POST['password']) : '';
	
	if($fname != '' && $sname != '' && $address != '' && $email != '' && $pass!= '')
	{
		if($phone == '')
		{
			$sql1 = "INSERT INTO userdetails (fname,sname,address,email) VALUES ('$fname','$sname','$address','$email')";
		}
		else
		{
			$sql1 = "INSERT INTO userdetails (fname,sname,address,email,phone) VALUES ('$fname','$sname','$address','$email','$phone')";
		}
		
		if (mysqli_query($conn, $sql1) === TRUE) {
			$sql2 = "SELECT id FROM userdetails WHERE email = '$email'";
			$result = mysqli_query($conn,$sql2);
			$row = mysqli_fetch_assoc($result);
			$id = $row['id'];

			$sql3 = "INSERT INTO credentials (id,pass) VALUES ('$id','$pass')";
			if (mysqli_query($conn, $sql3) === TRUE) {
				echo "You have successfully registered with us and your assigned user id is:<br> 
				<b>$id</b> 
				<br>
				Store it in a safe place as you will need it to log in.";
			} else {
				echo "Some error happened during registration, please try again.";
				include_once 'html/register.html';
			}
		}
		else 
		{
			echo "Please make sure your email or phone number are not already being used on this site.";
			include_once 'html/register.html';
		}
	}
	else
	{
		echo "An error occured during registration, please try again.";
		include_once 'html/register.html';
	}
	mysqli_close($conn);	
}
else
{
	include_once 'html/register.html';
}
echo "</div>";
include_once 'html/footer.html';
?>
</div>
</body>
</html>