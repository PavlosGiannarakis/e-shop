-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 28, 2018 at 04:00 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cw_wdd`
--

-- --------------------------------------------------------

--
-- Table structure for table `basket`
--

DROP TABLE IF EXISTS `basket`;
CREATE TABLE IF NOT EXISTS `basket` (
  `id` int(11) NOT NULL,
  `basket` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

DROP TABLE IF EXISTS `credentials`;
CREATE TABLE IF NOT EXISTS `credentials` (
  `id` int(11) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `staff` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credentials`
--

INSERT INTO `credentials` (`id`, `pass`, `staff`) VALUES
(100000, 'easy1', 0),
(100001, 'easy2', 0),
(100002, '1111', 0),
(100003, 'easy3', 0),
(100004, 'wic1', 0),
(100014, 'Pav1!', 0),
(100020, 'Pav1!', 0),
(100021, 'Pav1!', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `description`, `imageName`, `date_added`) VALUES
(1, 'Red Roses', 50, 'Lovely red rose bouquet. Perfect for special occasions.', 'ProductExample.jpg', '2018-11-21'),
(2, 'Mixed Summer Flowers', 35, 'Beautiful summer flowers that bring the summer feel with them. Perfect for a household decoration this summer.', 'ProductExample2.jpg', '2018-11-21'),
(3, 'Mixed Winter Flowers', 35, 'Beautiful winter flowers that bring the winter feel with them. Perfect for a household decoration this winter season.', 'ProductExample3.jpg', '2018-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

DROP TABLE IF EXISTS `userdetails`;
CREATE TABLE IF NOT EXISTS `userdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(10) NOT NULL,
  `sname` varchar(15) NOT NULL,
  `address` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=100022 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`id`, `fname`, `sname`, `address`, `email`, `phone`) VALUES
(100000, 'Pavlos', 'Giannarakis', '42 Howden Hall Drive', '40110403@live.napier.ac.uk', NULL),
(100001, 'Thea', 'Giannoudaki', '42 Howden Hall Drive', 'giannoudakitheano@gmail.com', NULL),
(100002, 'John', 'Doe', 'An Address', 'John.doe@doesntexist.com', '7874073354'),
(100003, 'Pavlos', 'Gian', 'My shed', 'pg@out.com', '07774855555'),
(100004, 'John', 'Snow', 'Winter is Coming', 'king@inthenorth.com', '07879745623'),
(100014, 'Pav', 'Me', '42 Howden Hall Drive', 'pm@me.com', ''),
(100020, 'Pav', 'G', '42 Howden Hall Drive', 'pg@gmail.caom', NULL),
(100021, 'Paul', 'X', 'House Drive', 'px@axa.com', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `basket`
--
ALTER TABLE `basket`
  ADD CONSTRAINT `fk_btid` FOREIGN KEY (`id`) REFERENCES `userdetails` (`id`);

--
-- Constraints for table `credentials`
--
ALTER TABLE `credentials`
  ADD CONSTRAINT `fk_id` FOREIGN KEY (`id`) REFERENCES `userdetails` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
